

INSERT INTO Customers(name,  address)
VALUES("Finkakor", "Helsingborg"), ("Småbröd", "Malmö"), ("Kaffebröd", "Landskrona"), 
("Bjudkakor", "Ystad"), ("Partykakor", "Kristianstad"), ("Gästkakor", "Hässleholm"), 
("Skånekakor", "Perstorp");

INSERT INTO RawMaterials(material_type)
VALUES("Fine-ground nuts")
, ("Ground, roasted nuts"),  ("Bread crumbs") , ("Egg whites"), ("Chocolate")
, ("Marzipan"), ("Eggs"), ("Potato starch"), ("Wheat flour")
, ("Chopped almonds"), ("Cinnamon"), ("Vanilla sugar");

INSERT INTO RawMaterials(material_type, total_quantity)
VALUES("Flour", 3210000), ("Butter", 4210000), ("Icing sugar", 2130000), ("Roasted, chopped nuts", 2190000)
, ("Sugar", 2190000), ("Sodium bicarbonate", 2190000), ("Vanilla", 2190000);

INSERT INTO Recipes(name)
VALUES("Nut ring"), ("Nut cookie"),("Amneris"),("Tango"),("Almond delight"),("Berliner");

INSERT INTO RecipeItems(quantity, material_type, recipe_name)
VALUES(450, "Flour", "Nut ring"), (450, "Butter", "Nut ring"),(190, "Icing sugar", "Nut ring"),
(225, "Roasted, chopped nuts", "Nut ring");

INSERT INTO RecipeItems(quantity, material_type, recipe_name)
VALUES(750, "Fine-ground nuts", "Nut cookie"),(625, "Ground, roasted nuts", "Nut cookie"),
(125, "Bread crumbs", "Nut cookie"),(375, "Sugar", "Nut cookie"),
(3.5, "Egg whites", "Nut cookie"),(50, "Chocolate", "Nut cookie");

INSERT INTO RecipeItems(quantity, material_type, recipe_name)
VALUES(750, "Marzipan", "Amneris"),(250, "Butter", "Amneris"),
(250, "Eggs", "Amneris"),(25, "Potato starch", "Amneris"),(25, "Wheat flour", "Amneris");

INSERT INTO RecipeItems(quantity, material_type, recipe_name)
VALUES(200, "Butter", "Tango"), (250, "Sugar", "Tango"),(300, "Flour", "Tango"),
(4, "Sodium bicarbonate", "Tango"),(2, "Vanilla", "Tango");

INSERT INTO RecipeItems(quantity, material_type, recipe_name)
VALUES(400, "Butter", "Almond delight"),(270, "Sugar", "Almond delight"),(279, "Chopped almonds", "Almond delight"),
(400, "Flour", "Almond delight"),(10, "Cinnamon", "Almond delight");

INSERT INTO RecipeItems(quantity, material_type, recipe_name)
VALUES(350, "Flour", "Berliner"), (250, "Butter", "Berliner"), (100, "Icing sugar", "Berliner"),
(50, "Eggs", "Berliner"),  (5, "Vanilla sugar", "Berliner"), (50, "Chocolate", "Berliner"); 
