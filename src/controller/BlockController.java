package controller;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import model.Database;

public class BlockController {
	private Database database;
	private Stage dialogStage;
	private ObservableList<String> blockOptions;

	@FXML
	private ChoiceBox<String> blockChoiceBox;
	@FXML
	private DatePicker blockTo;
	@FXML
	private DatePicker blockFrom;

	public void initialize() {
		LocalDate today = LocalDate.now();
		LocalDate yesterday = today.minus(1, ChronoUnit.DAYS);
		blockFrom.setValue(yesterday);
		blockTo.setValue(today);
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setDatabase(Database database) {
		this.database = database;
		blockOptions = database.getRecipes();
		blockChoiceBox.setItems(blockOptions);
		blockChoiceBox.getSelectionModel().select(0);
	}

	@FXML
	private void blockButtonClicked() {
		if (!blockFrom.getValue().isAfter(blockTo.getValue())) {
			String blockProduct = blockChoiceBox.getValue();
			database.blockPalletsWithCookieType(blockProduct, java.sql.Date.valueOf(blockFrom.getValue()),
					java.sql.Date.valueOf(blockTo.getValue()));
			dialogStage.close();
		} else {
			Alert alert = new Alert(Alert.AlertType.WARNING);
			alert.initOwner(dialogStage);
			alert.setTitle("Incorrect dates");
			alert.setHeaderText("Incorrect dates");
			alert.setContentText("Please make sure that date the interval is valid.");
			alert.showAndWait();
		}
	}

	@FXML
	private void cancelButtonClicked() {
		dialogStage.close();
	}

}
