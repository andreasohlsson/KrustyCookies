package controller;

import main.MainApp;
import model.Database;
import model.Pallet;


import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class MainController {
	@FXML
	private TableView<Pallet> palletTable;
	@FXML
	private TableColumn<Pallet, String> palletTypeColumn;
	@FXML
	private TableColumn<Pallet, String> palletIdColumn;
	@FXML
	private Label palletTypeLabel;
	@FXML
	private Label palletIdLabel;
	@FXML
	private Label palletLocationLabel;
	@FXML
	private Label palletCreatedAtLabel;
	@FXML
	private Label palletDeliveredLabel;
	@FXML
	private Label palletDeliveryLabel;
	@FXML
	private Label palletCustomerLabel;
	@FXML
	private Label palletBlockedLabel;
	@FXML
	private ChoiceBox<String> queryBox;
	@FXML
	private TextField queryInput;

	private ObservableList<String> queryOptions;

	private MainApp main;
	private Database database;
	private ObservableList<Pallet> pallets;

	/**
	 * Is called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	public void setMain(MainApp main) {
		this.main = main;
	}

	public void setDatabase(Database database) {
		this.database = database;
		initTableView();
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	public void initialize() {
		palletTypeColumn.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
		palletIdColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty());
		palletTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showPalletDetail(newValue));
		palletTable.getSelectionModel().select(0);
		queryOptions = FXCollections.observableArrayList("Pallet id", "Product type",
				"Produced within (yyyy-mm-dd yyyy-mm-dd)", "Customer", "Blocked");
		queryBox.setItems(queryOptions);
		queryBox.getSelectionModel().select(0);
	}

	/**
	 * Fills the TableView with data
	 */
	private void initTableView() {
		// fetch all pallets
		if (database == null) {
			System.out.println("database null");
		}

		pallets = database.fetchPallets();
		palletTable.setItems(pallets);
		palletTable.getSelectionModel().select(0);
	}

	private void showPalletDetail(Pallet pallet) {
		if (pallet != null) {
			palletTypeLabel.setText(pallet.getType());
			palletIdLabel.setText(pallet.getId());
			palletLocationLabel.setText(pallet.getLocation());
			palletCreatedAtLabel.setText(pallet.getCreatedAt());
			palletDeliveryLabel.setText(pallet.getDeliveryDate());
			palletDeliveredLabel.setText(pallet.getDeliveredAt());
			palletCustomerLabel.setText(pallet.getCustomerName());
			palletBlockedLabel.setText(pallet.getBlocked());
		} else {
			palletTypeLabel.setText("");
			palletIdLabel.setText("");
			palletLocationLabel.setText("");
			palletCreatedAtLabel.setText("");
			palletDeliveryLabel.setText("");
			palletDeliveredLabel.setText("");
			palletCustomerLabel.setText("");
			palletBlockedLabel.setText("");

		}
	}

	@FXML
	private void blockClicked() {
		boolean block = main.showBlockView();
		if (block) {
			ObservableList<Pallet> newPallets = FXCollections.observableArrayList();
			for (Pallet pallet : pallets) {
				Pallet newPallet = database.getPalletInformation(Integer.valueOf(pallet.getId()));
				newPallets.add(newPallet);
			}
			pallets = newPallets;
			palletTable.setItems(pallets);
			palletTable.getSelectionModel().select(0);
		}
	}

	@FXML
	private void newPalletClicked() {
		List<String> choices = database.getRecipes();
		ChoiceDialog<String> dialog = new ChoiceDialog<>("Nut ring", choices);
		dialog.setTitle("New Pallet");
		dialog.setHeaderText("Choose which type of product \nthe pallet should contain");
		dialog.setContentText("Product type:");
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(cookie -> addPallet(cookie));
	}

	private void addPallet(String cookie) {
		boolean success = database.createNewPallet(cookie);
		if (success) {
			// fetch all pallets and update view
			initTableView();
		} else {
			Alert alert = new Alert(Alert.AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("Pallet could not be created");
			alert.setHeaderText("Pallet could not be created");
			alert.setContentText("Please try to create another type of pallet");
			alert.showAndWait();
		}
	}

	private boolean isNumeric(String id) {
		return id.matches("-?\\d+(\\.\\d+)?");
	}

	@FXML
	private void searchButtonClicked() {
		String queryType = queryBox.getValue();
		switch (queryType) {
		case "Pallet id":
			String palletId = queryInput.getText();
			if (!palletId.equals("") && isNumeric(palletId)) {
				Pallet pallet = database.getPalletInformation(Integer.valueOf(palletId));
				pallets.clear();
				pallets.add(pallet);
				palletTable.getSelectionModel().select(0);
			} else {
				Alert alert = new Alert(Alert.AlertType.WARNING);
				alert.initOwner(main.getPrimaryStage());
				alert.setTitle("Search error");
				alert.setHeaderText("The search could not be completed.");
				alert.setContentText("Make sure that the search input is an integer.");
				alert.showAndWait();
			}
			break;
		case "Product type":
			String productType = queryInput.getText();
			if (!productType.equals("")) {
				ObservableList<Pallet> searchResults = database.getPalletsWithType(productType);
				pallets.clear();
				pallets.addAll(searchResults);
				palletTable.getSelectionModel().select(0);
			}
			break;
		case "Produced within (yyyy-mm-dd yyyy-mm-dd)":
			String query = queryInput.getText();

			try {
				String dateStart = query.split(" ")[0];
				String dateEnd = query.split(" ")[1];
				System.out.println("datestart: " + dateStart);
				System.out.println("dateend: " + dateEnd);

				java.util.Date ds = new SimpleDateFormat("yyyy-MM-dd").parse(dateStart);
				java.util.Date de = new SimpleDateFormat("yyyy-MM-dd").parse(dateEnd);

				java.sql.Date sqlStartDate = new java.sql.Date(ds.getTime());
				java.sql.Date sqlEndDate = new java.sql.Date(de.getTime());
				ObservableList<Pallet> searchResult = database.getPalletsBetweenDates(sqlStartDate, sqlEndDate);
				pallets.clear();
				pallets.addAll(searchResult);
				palletTable.getSelectionModel().select(0);

			} catch (Exception e) {
				Alert alert = new Alert(Alert.AlertType.WARNING);
				alert.initOwner(main.getPrimaryStage());
				alert.setTitle("Search error");
				alert.setHeaderText("The search could not be completed.");
				alert.setContentText("Make sure that the query format is: yyyy-mm-dd yyyy-mm-dd");
				alert.showAndWait();
				break;
			}

			break;
		case "Customer":
			String customerName = queryInput.getText();
			ObservableList<Pallet> searchResult = database.getPalletsToCustomer(customerName);
			pallets.clear();
			pallets.addAll(searchResult);
			palletTable.getSelectionModel().select(0);
			break;
		case "Blocked":
			ObservableList<Pallet> blockedPallets = database.getBlockedPallets();
			pallets.clear();
			pallets.addAll(blockedPallets);
			palletTable.getSelectionModel().select(0);
			break;
		default:
		}
	}

	@FXML
	private void resetButtonClicked() {
		initTableView();
	}
}