package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Pallet {
	private StringProperty palletId;
	private StringProperty location;
	private StringProperty createdAt;
	private StringProperty type;
	private StringProperty customerName;
	private StringProperty deliveryDate;
	private StringProperty deliveredAt;
	private StringProperty blocked;
	
	
	public Pallet() {
		palletId = new SimpleStringProperty("");
		type = new SimpleStringProperty("");
		createdAt = new SimpleStringProperty("");
		location = new SimpleStringProperty("");
		deliveryDate = new SimpleStringProperty("");
		customerName = new SimpleStringProperty("");
		deliveredAt = new SimpleStringProperty("");
		blocked = new SimpleStringProperty("");
	}
	
	public void setId(String id) {
		this.palletId.set(id);
	}
	
	public String getId() {
		return palletId.get();
	}
	
	public StringProperty idProperty() {
		return palletId;
	}
	
	public void setType(String type) {
		this.type.set(type);
	}
	
	public String getType() {
		return type.get();
	}
	
	public StringProperty typeProperty() {
		return type;
	}
	
	public void setLocation(String location) {
		this.location.set(location);
	}
	
	public String getLocation() {
		return location.get();
	}
	
	public StringProperty locationProperty() {
		return location;
	}
	
	public void setCustomerName(String customerName) {
		this.customerName.set(customerName);
	}
	
	public String getCustomerName() {
		return customerName.get();
	}
	
	public StringProperty customerNameProperty() {
		return customerName;
	}
	
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate.set(deliveryDate);
	}
	
	public String getDeliveryDate() {
		return deliveryDate.get();
	}
	
	public StringProperty deliveryDateProperty() {
		return deliveryDate;
	}
	
	public void setDeliveredAt(String deliveredAt) {
		this.deliveredAt.set(deliveredAt);
	}
	
	public String getDeliveredAt() {
		return deliveredAt.get();
	}
	
	public StringProperty deliveredAtProperty() {
		return deliveredAt;
	}
	
	public void setCreatedAt(String createdAt) {
		this.createdAt.set(createdAt);
	}
	
	public String getCreatedAt() {
		return createdAt.get();
	}
	
	public StringProperty createdAtProperty() {
		return createdAt;
	}
	
	public void setBlocked(String blocked) {
		this.blocked.set(blocked);
	}
	
	public String getBlocked() {
		return blocked.get();
	}
	
	public StringProperty blockedProperty() {
		return blocked;
	}
	
}
