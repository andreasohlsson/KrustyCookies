package main;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import model.Database;
import javafx.scene.Group;
import javafx.scene.Scene;
import controller.BlockController;
import controller.MainController;

public class MainApp extends Application {

	private final String DB_NAME = "krustyCookies.db.sqlite";
	private Stage primaryStage;
	private BorderPane window;
	private Database database;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("Krusty Cookies");
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ParentPane.fxml"));
			window = (BorderPane) loader.load();
			Scene scene = new Scene(window, 812, 500);
			Stage dialogStage = new Stage();
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.setScene(scene);
			primaryStage.setScene(scene);
			primaryStage.show();
			database = new Database();
			database.openConnection(DB_NAME);
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				@Override
				public void handle(WindowEvent e) {
					database.closeConnection();
					Platform.exit();
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		showMainView();
	}

	public void showMainView() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Main.fxml"));
			AnchorPane anchorPane = (AnchorPane) loader.load();
			window.setCenter(anchorPane);
			MainController controller = loader.getController();
			controller.setMain(this);
			controller.setDatabase(database);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean showBlockView() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Block.fxml"));
			AnchorPane anchorPane = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Block Product");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(anchorPane);
			dialogStage.setScene(scene);

			BlockController controller = loader.getController();
			controller.setDatabase(database);
			controller.setDialogStage(dialogStage);

			// Show the dialog
			dialogStage.showAndWait();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
