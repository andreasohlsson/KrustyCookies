# EDAF75, project report

This is the report for

 + Andreas Ohlsson, dat14aoh
 + Fredric Billow, elt13fbi

We solved this project on our own, except for:

 + The Peer-review meeting

## ER-design

The model is in the file [`er-model.png`](er-model.png):

![er-model](er-model.png)


## Relations

+ RawMaterials(**material_type**, total_quantity, last_delivery_date, last_delivery_amount)
+ RecipeItems(**id**, _material_type_, _recipe_name_, quantity)
+ Recipes(**name**)
+ OrderItems(**id**, _recipe_name_, _order_id_, quanitity)
+ Orders(**id**, delivery_date, delivered_at, _customer_id_, _loading_bill_id_)
+ Customers(**id**, name, address)
+ OrderPallets(**id**, _order_id_, _pallet_id_)
+ Pallets(**id**, _recipe_name_, created_at, location, blocked)

## Scripts to set up database

The scripts used to set up and populate the database are in:

 + [`create-schema.sql`](create-schema.sql) (defines the tables), and
 + [`initial-data.sql`](initial-data.sql) (inserts data).

So, to create and initialize the database, we run:

```shell
sqlite3 krustyCookies.db.sqlite < create-schema.sql
sqlite3 krustyCookies.db.sqlite < initial-data.sql
```

## How to compile and run the program

In the root directory where the source files are, we run:

```shell
mkdir bin
javac -d bin/ -cp src src/main/MainApp.java
cp -R src/view bin/view
java -cp bin:sqlite-jdbc.jar main.MainApp
```

